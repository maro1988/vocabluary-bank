package pl.maro.vocabularybank;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import pl.maro.vocabularybank.domain.Word;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static pl.maro.vocabularybank.SheetService.createSheet;
import static pl.maro.vocabularybank.SheetService.totalList;
import static pl.maro.vocabularybank.SubtitlesParser.loadTextFile;

public class App {


    public static void main(String[] args) throws IOException, InvalidFormatException {
        List<String> total = totalList();
        String fileName = "the-lord-of-the-rings-the-fellowship-of-the-ring-english.srt";
        Map<String, Word> words = loadTextFile(fileName, total);
        createSheet(words, fileName);
        System.out.println(total.size());
    }


}
