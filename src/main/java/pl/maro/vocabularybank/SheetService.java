package pl.maro.vocabularybank;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pl.maro.vocabularybank.domain.Word;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SheetService {

    private static String[] columns = {"word", "distribution"};

    public static void createSheet(Map<String, Word> words, String fileName) throws IOException {

        Workbook workbook = new XSSFWorkbook();
        CreationHelper creationHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Main");
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFillBackgroundColor(IndexedColors.YELLOW1.getIndex());
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        int rowNum = 1;
        for (String key : words.keySet()) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(words.get(key).getText());
            row.createCell(1).setCellValue(words.get(key).getDistribution());
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        FileOutputStream fileOut = new FileOutputStream("src/main/resources/" + fileName + ".xls");
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }


    public static List<String> totalList() throws IOException, InvalidFormatException {
        List<String> total = new ArrayList<>();
        File dir = new File("src/main/resources");
        File[] files = dir.listFiles();
        for (File file : files) {
            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = sheet.iterator();
            int rows = sheet.getPhysicalNumberOfRows();
            for (int i = 1; i < rows; i++) {
                Row currentRow = iterator.next();
                Cell cell = currentRow.getCell(0);
                total.add(cell.getStringCellValue());
            }
        }
        return total;
    }
}
