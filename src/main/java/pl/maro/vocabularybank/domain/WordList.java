package pl.maro.vocabularybank.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WordList {
    private String title;
    private int amountOfWords;
    private int amountOfSeparateWords;
}
