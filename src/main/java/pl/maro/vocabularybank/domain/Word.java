package pl.maro.vocabularybank.domain;




public class Word {
    private String text;
    private int distribution = 1;

    public Word(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getDistribution() {
        return distribution;
    }

    public void addDistribution(){
        this.distribution++;
    }
}
