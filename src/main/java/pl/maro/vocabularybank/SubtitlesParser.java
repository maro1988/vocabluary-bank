package pl.maro.vocabularybank;

import pl.maro.vocabularybank.domain.Word;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class SubtitlesParser {
    public static Map<String, Word> loadTextFile(String path, List<String> total) {
        Map<String, Word> map = new HashMap();
        try {
            var data = new String(Files.readAllBytes(Paths.get(path)));
            var processedData = replace(data.toLowerCase()).trim();
            List<String> list = new ArrayList(Arrays.asList(processedData.split(" ")));
            System.out.println(list.size());
            list.removeAll(total);
            System.out.println(list.size());
            list.forEach(word -> {
                if (map.get(word) != null) {
                    map.get(word).addDistribution();
                } else {
                    map.put(word, new Word(word));
                }
            });
            System.out.println(map.size());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    private static String replace(String data) {
        try {
            String str = data.codePoints()
                    .filter(x -> x == 32 || x == 10 || x == 39 || x >= 97 && x <= 122)
                    .collect(StringBuilder::new,
                            (StringBuilder::appendCodePoint),
                            StringBuilder::append)
                    .toString()
                    .replace("\n", " ")
                    .replaceAll(" {2,}", " ");

            return str;
        } catch (Exception e) {
            return "dupa";
        }
    }

}
